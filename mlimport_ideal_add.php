<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Mantas Lukosius <info@elpresta.eu>
 * @copyright 2015-2016 elPresta
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require(dirname(__FILE__) . "/../../config/config.inc.php");
require(dirname(__FILE__) . "/../../init.php");
require_once(dirname(__FILE__) . "/MLimModifiedHelpers.php");
require_once(dirname(__FILE__) . "/classes/IdealLuxWorker.php");
require_once(dirname(__FILE__) . "/classes/MwHandelWorker.php");

$start = microtime(true);

$workers = [new IdealLuxWorker()/*, new MwHandelWorker()*/];

echo "CRON START... <br />";


foreach ($workers as $worker)
{
    $worker->work();
}

$time_spent = round((microtime(true) - $start), 2);
echo "Done. Check execution statistics to get more info. Time spent: " . round((microtime(true) - $start), 2) . " s.";
