<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Mantas Lukosius <info@elpresta.eu>
 *  @copyright 2015-2016 elPresta
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

include(dirname(__FILE__) . '/../../config/config.inc.php');
include(dirname(__FILE__) . '/../../init.php');
include(dirname(__FILE__) . '/mlgitoma.php');

echo date("Y-m-d H:i:s");
$mlgitoma = new Mlgitoma();
$mlgitoma->downloadData();
echo date("Y-m-d H:i:s");

