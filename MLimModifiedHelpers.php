<?php

/**
 * Created by PhpStorm.
 * User: mgrid
 * Date: 2017-06-15
 * Time: 19:28
 */
class MLimModifiedHelpers
{
    static function saveSupplier($myProduct)
    {
        $id_product_supplier = ProductSupplier::getIdByProductAndSupplier((int)$myProduct->id, 0, (int)$myProduct->id_supplier);
        if ($id_product_supplier)
        {
            $product_supplier = new ProductSupplier((int)$id_product_supplier);
        }
        else
        {
            $product_supplier = new ProductSupplier();
        }

        $product_supplier->id_product = $myProduct->id;
        $product_supplier->id_product_attribute = 0;
        $product_supplier->id_supplier = $myProduct->id_supplier;
        $product_supplier->product_supplier_price_te = $myProduct->wholesale_price;
        //$product_supplier->product_supplier_reference = $myProduct->supplier_reference;
        $product_supplier->save();
        return $product_supplier;
    }

    static function addImage($url, $id_product)
    {
        if (Shop::isFeatureActive())
        {
            Shop::setContext(Shop::CONTEXT_ALL);
        }

        $product_has_images = (bool)Image::getImages(1, (int)$id_product);
        if (!empty($url))
        {
            $image = new Image();
            $image->id_product = (int)$id_product;
            $image->position = Image::getHighestPosition($id_product) + 1;
            $image->cover = (!$product_has_images) ? true : false;
            if ($image->add())
            {
                //associateImageToShop((int)$id_product, (int)$image->id);
                static::CopyImg($id_product, $image->id, $url, 'products');
            }
        }
    }

    static function copyImg($id_entity, $id_image = null, $url, $entity = 'products')
    {
        $tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');
        $watermark_types = explode(',', Configuration::get('WATERMARK_TYPES'));

        switch ($entity)
        {
            default:
            case 'products':
                $imageObj = new Image($id_image);
                $path = $imageObj->getPathForCreation();
                break;
            case 'categories':
                $path = _PS_CAT_IMG_DIR_ . (int)($id_entity);
                break;
        }
        $url_source_file = str_replace(' ', '%20', trim($url));
        $data = static::fileGetContentsCurl($url_source_file);
        if ($data == '')
        {
            return false;
        }
        if (@file_put_contents($tmpfile, $data))
        {
            ImageManager::resize($tmpfile, $path . '.jpg');
            $imagesTypes = ImageType::getImagesTypes($entity);
            foreach ($imagesTypes as $k => $imageType)
            {
                ImageManager::resize($tmpfile, $path . '-' . stripslashes($imageType['name']) . '.jpg', $imageType['width'], $imageType['height']);
            }

            if (in_array($imageType['id_image_type'], $watermark_types))
            {
                Module::hookExec('watermark', array('id_image' => $id_image, 'id_product' => $id_entity));
            }

        }
        else
        {
            unlink($tmpfile);
            return false;
        }
        unlink($tmpfile);
        return true;
    }

    static function fileGetContentsCurl($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    static function addFeature($ft, $id_product)
    {

        // Import en values. Translations doing from BO.
        $en_lang = 1;

        $feature_name = $ft['savybes_pavadinimas'];
        $feature_value = $ft['savybes_pavadinimas'];

        if (!empty($feature_name) AND !empty($feature_value))
        {
            $id_feature = (int)static::getFeatureValueByName($feature_name, $en_lang);
            if (!$id_feature)
            {
                $id_feature = Feature::addFeatureImport($feature_name, false);
            }

            $id_feature_value = static::getFeatureValueId($feature_value, $id_feature);
            if (!$id_feature_value)
            {
                $id_feature_value = static::addFeatureValueN($id_feature, $feature_value);
            }

            // Assign
            Product::addFeatureProductImport($id_product, $id_feature, $id_feature_value);
        }


    }

    static function getFeatureValueByName($feature, $lang)
    {
        $sql = 'SELECT id_feature FROM ' . _DB_PREFIX_ . 'feature_lang WHERE id_lang = ' . $lang . ' AND `name` = \'' . pSQL($feature) . '\'';
        $idfeature = (int)Db::getInstance()->getValue($sql);

        return $idfeature;
    }

    static function getFeatureValueId($savybe, $id_lang)
    {
        $sql = 'SELECT id_feature_value FROM ' . _DB_PREFIX_ . 'feature_value_lang WHERE id_lang = ' . (int)$id_lang . ' AND `value` = \'' . pSQL($savybe) . '\'';
        $id_feature_value = Db::getInstance()->getValue($sql);

        return $id_feature_value;
    }

    static function addFeatureValueN($id_feature, $name)
    {
        $name = static::replaceDisallowed($name, null);

        $rq = Db::getInstance()->executeS('
			SELECT fv.`id_feature_value`
			FROM ' . _DB_PREFIX_ . 'feature_value fv
			LEFT JOIN ' . _DB_PREFIX_ . 'feature_value_lang fvl
				ON (fvl.`id_feature_value` = fv.`id_feature_value`)
			WHERE `value` = \'' . pSQL($name) . '\'
				AND fv.`id_feature` = ' . (int)$id_feature . '
			GROUP BY fv.`id_feature_value` LIMIT 1');

        if (!isset($rq[0]['id_feature_value']) || !$id_feature_value = (int)$rq[0]['id_feature_value'])
        {
            // Feature doesn't exist, create it
            $feature_value = new FeatureValue();
            $languages = Language::getLanguages();

            foreach ($languages as $language)
            {
                $feature_value->value[$language['id_lang']] = strval($name);
            }

            $feature_value->id_feature = (int)$id_feature;
            $feature_value->custom = 0;
            $feature_value->add();
            $id_feature_value = (int)$feature_value->id;
        }

        return (int)$id_feature_value;
    }

    static function replaceDisallowed($str, $allowedLenght)
    {
        if ($allowedLenght == null)
        {
            $allowedLenght = mb_strlen($str);
        }
        $co = array("<", ">", ";", "=", "#", "{", "}");
        $cim = array("-", "-", ",", "-", "*", "[", "]");
        return mb_substr(str_replace($co, $cim, trim($str)), 0, $allowedLenght);
    }

    public static function downloadData($path, $type)
    {
        // GLOBAL VARIABLE TO LOAD DATA PATH
        //$path = Configuration::get("mlimport_ideal_data");
        if (static::curlDownload($path, $type))
        {
            static::addLog(0, "${type} DATA downloaded!;");
            return true;
        }
        static::addLog(0, "Error - failed to download ${type} data;");
        return false;
    }

    /*
     * Download data from content and put to data folder
     */

    public static function curlDownload($url, $full_place)
    {
        $data = static::file_get_contents_curl($url);
        $handle = fopen($full_place."1", "w");

        if (fwrite($handle, $data))
        {
            Configuration::updateValue("mlimport_ideal_cdtime", date("Y-m-d H:i:s"));
            fclose($handle);
            unlink($full_place);
            rename($full_place."1", $full_place);
            return true;
        }

        fclose($handle);
        return false;

    }


    /*
     * Add manufacturer
     */

    public static function file_get_contents_curl($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);

        $data = curl_exec($ch);
        if(!$data)
        {
            $curl_error = curl_error($ch);
            static::addLog(0, "Failed to download data: $curl_error");
        }
        curl_close($ch);


        return $data;
    }

    /*
     * Add supplier
     */

    public static function addLog($id_product, $log_text)
    {
        $now = date("Y-m-d H:i:s");
        Db::getInstance()->insert("mlimport_ideal", array(
            "id_product" => $id_product,
            "log_info" => pSQL($log_text),
            "date_add" => pSQL($now),
        ));
        echo $log_text;
        echo "\r\n<br>";
    }



    public static function addManufacturer($gamintojas)
    {
        if(static::cacheContains("manufacturer", $gamintojas))
            return static::$cache["manufacturer"][$gamintojas];
        $idManufacturer = Manufacturer::getIdByName($gamintojas);

        if ($idManufacturer === false)
        {
            $newManufacturer = new Manufacturer();
            $newManufacturer->name = $gamintojas;
            $newManufacturer->link_rewrite = static::createMultiLangField(Tools::link_rewrite($gamintojas));
            $newManufacturer->active = 1;
            $newManufacturer->add();
            $idManufacturer = $newManufacturer->id;
        }
        static::cache("manufacturer", $gamintojas, $idManufacturer);
        return $idManufacturer;
    }

    private static function cache($type, $key, $value)
    {
        if(!isset(static::$cache[$type]))
            static::$cache[$type] = [];
        static::$cache[$type][$key] = $value;
    }

    private static function cacheContains($type, $key)
    {
        if(!isset(static::$cache[$type]))
            static::$cache[$type] = [];
        return array_key_exists($key, static::$cache[$type]);
    }

    private static $cache = ["manufacturer" => [], "supplier" => []];




    public static function createMultiLangField($field)
    {
        $languages = Language::getLanguages(false);

        $res = array();
        foreach ($languages as $lang)
        {
            $res[$lang['id_lang']] = $field;
        }

        return $res;
    }

    /*
     * Log action to DB
     */

    public static function addSupplier($xSUPPLIER)
    {
        if(static::cacheContains("supplier", $xSUPPLIER))
            return static::$cache["supplier"][$xSUPPLIER];
        $idSupplier = Supplier::getIdByName($xSUPPLIER);
        if($idSupplier === false)
        {
            $newSupplier = new Supplier();
            $newSupplier->name = $xSUPPLIER;
            $newSupplier->link_rewrite = static::createMultiLangField(Tools::link_rewrite($xSUPPLIER));
            $newSupplier->active = 1;
            $newSupplier->add();
            $idSupplier = $newSupplier->id;
        }
        static::cache("supplier", $xSUPPLIER, $idSupplier);
        return $idSupplier;
    }

    /*
     * Get ID by CODE
     * return id product
     */

    public static function getIdByCode($code)
    {
        $sql = "SELECT `id_product` FROM `" . _DB_PREFIX_ . "product` WHERE `reference` LIKE \"" . pSQL($code) . "\"";
        $id_product = (int)Db::getInstance()->getValue($sql);

        if ($id_product)
        {
            return $id_product;
        }

        return false;
    }

    /*
     * Read CSV to array
     */
    public static function readCsvFile($path, $separator, $skipLines = 1)
    {
        // Main method to load new data
        $handle = fopen($path, "r");
        if ($handle !== FALSE)
        {
            # Set the parent multidimensional array key to 0.
            $csvarray = array();
            for ($i = 0; $i < $skipLines; $i++)
            {
                fgetcsv($handle, null, $separator);
            } // skips headers
            while (($data = fgetcsv($handle, null, $separator)) !== FALSE)
            {
                $csvarray[] = $data;
            }
            fclose($handle);
            return $csvarray;
        }
        else
        {
            static::addLog(0, "$path DATA cant be loaded;");
            return false;
        }
    }

    /**
     * Analyze CSV file content
     * @param $file
     * @deprecated handled by workers in their implementation
     */
    public static function AnalyseFileCsv($file, $capture_limit_in_kb = 10)
    {
        // capture starting memory usage
        $output["peak_mem"]["start"] = memory_get_peak_usage(true);
        $output["read_kb"] = $capture_limit_in_kb;

        // read in file
        $fh = fopen($file, "r");
        $contents = fread($fh, ($capture_limit_in_kb * 1024)); // in KB
        fclose($fh);

        // specify allowed field delimiters
        $delimiters = array(
            "comma" => ",",
            "semicolon" => ";",
            "tab" => "\t",
            "pipe" => "|",
            "colon" => ":"
        );

        // specify allowed line endings
        $line_endings = array(
            "rn" => "\r\n",
            "n" => "\n",
            "r" => "\r",
            "nr" => "\n\r"
        );

        // loop and count each line ending instance
        foreach ($line_endings as $key => $value)
        {
            $line_result[$key] = substr_count($contents, $value);
        }

        // sort by largest array value
        asort($line_result);

        // log to output array
        $output["line_ending"]["results"] = $line_result;
        $output["line_ending"]["count"] = end($line_result);
        $output["line_ending"]["key"] = key($line_result);
        $output["line_ending"]["value"] = $line_endings[$output["line_ending"]["key"]];
        $lines = explode($output["line_ending"]["value"], $contents);

        // remove last line of array, as this maybe incomplete?
        array_pop($lines);

        // create a string from the legal lines
        $complete_lines = implode(" ", $lines);

        // log statistics to output array
        $output["lines"]["count"] = count($lines);
        $output["lines"]["length"] = strlen($complete_lines);

        // loop and count each delimiter instance
        foreach ($delimiters as $delimiter_key => $delimiter)
        {
            $delimiter_result[$delimiter_key] = substr_count($complete_lines, $delimiter);
        }

        // sort by largest array value
        asort($delimiter_result);

        // log statistics to output array with largest counts as the value
        $output["delimiter"]["results"] = $delimiter_result;
        $output["delimiter"]["count"] = end($delimiter_result);
        $output["delimiter"]["key"] = key($delimiter_result);
        $output["delimiter"]["value"] = $delimiters[$output["delimiter"]["key"]];

        // capture ending memory usage
        $output["peak_mem"]["end"] = memory_get_peak_usage(true);
        return $output;
    }
}