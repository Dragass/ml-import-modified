<?php
/**
 * Created by PhpStorm.
 * User: mgrid
 * Date: 2017-06-15
 * Time: 18:55
 */

require_once(dirname(__FILE__) . "/AbstractProduct.php");

/**
 * Class IdeaLuxProduct
 *
 * Implemented in regards to IdeaLux CSV scheme.
 */
class IdeaLuxProduct extends AbstractProduct
{
    private $code = null; // for caching purposes
    private $imagesArray = null;
    private $name = null;
    private $categoryId = null;
    private $categories = null;

    function getImagesArray()
    {
        if ($this->imagesArray == null)
        {
            $this->imagesArray = [$this->backingArray[40]];
        }

        return $this->imagesArray;
    }

    function getReference()
    {
        return $this->getCode();
    }

    function getCode()
    {
        if ($this->code == null)
        {
            $product_code = (string)substr($this->backingArray[0], -5);
            $this->code = static::getPrefix() . '-' . $product_code;
        }
        return $this->code;
    }

    static function getPrefix()
    {
        return "IL";
    }

    function getModifiedPrice()
    {
        return round(($this->getPrice() / 1.21), 6);
    }

    function getPrice()
    {
        return (float)$this->backingArray[35];
    }

    function getMLSKU()
    {
        return $this->backingArray[0];
    }

    function getQuantity()
    {
        return (int)$this->backingArray[6];
    }

    function getName()
    {
        if ($this->name == null)
        {
            $this->name = str_replace("_", " ", $this->backingArray[1]);
        }
        return $this->name;
    }

    function getDescription()
    {
        return MLimModifiedHelpers::createMultiLangField('<!--' . $this->backingArray[2] . '-->');
    }

    function getCategories()
    {
        $this->getCategoryId();
        return $this->categories;
    }

    function getCategoryId()
    {
        if ($this->categoryId == null)
        {
            $sql = 'SELECT selected_cat FROM ' . _DB_PREFIX_ . 'mlimport_ideal_cats WHERE category_code LIKE "' . trim($this->backingArray[5]) . '" ';
            $res = Db::getInstance()->getValue($sql);

            if ($res)
            {
                $this->categories = explode(',', $res);
                $this->categoryId = end($this->categories);
            }

            // If no categories just assign default ID
            if (!Validate::isInt($this->categoryId))
            {
                $this->categoryId = 2;
                $this->categories = array(2);
            }
        }
        return $this->categoryId;
    }

    function isValid()
    {
        return isset($this->backingArray[0]) && $this->backingArray[0] > 0;
    }

    function getFeaturesArray()
    {
        return [];
    }

    public function getManufacturerId()
    {
        return MLimModifiedHelpers::addManufacturer("Ideal Lux s.r.l");
    }

    public function getSupplierId()
    {
        return MLimModifiedHelpers::addSupplier("Ideal lux");
    }

    function getWeight()
    {
        return (float)$this->backingArray[27];
    }
}