{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel">
    <h3><i class="icon icon-credit-card"></i> {l s='IDEAL LUX XML' mod='mlimport_ideal'}</h3>
    <p>
        {l s='Ideal LUX XML importavimo modulis.' mod='mlimport_ideal'}
    </p>
</div>

<div class="panel">
    <h3>Nuorodos</h3>
    <p>
        <span>CRON pridėjimas: <a target="_blank" href="{$add_url}">{$add_url}</a> </span> <br/>
        <span>CRON atnaujinimas: <a target="_blank" href="{$update_url}">{$update_url} </a> </span> <br/>
    </p>
    <hr/>

    <p>Svetainė: Topolina.lt</p>
    <p>Supplier: IDEAL LUX</p>
    <p>Server execution time: <b>{$footime}</b></p>
    <p>Short description limitas: <b>{$sl}</b></p>
    <p>Allow iframe: <b>{$ai} </b></p>
    <p>{$jecurl}</p>
</div>

<div class="panel info-zurnalas" style="height: 400px;">
    <h3><i class="icon icon-credit-card"></i> {l s='Kategorijų suvedžiojimas' mod='mlimport_ideal'}</h3>
    <form method="post" name="submitIdealCategories"
          action="index.php?controller=AdminModules&configure=mlimport_ideal&module_name=mlimport_ideal&token={$token|escape:'htmlall':'UTF-8'}">
        <input type="hidden" name="submitIdealCats" value="1">
        <input type="submit" class="btn button large" value="{l s='Išsaugoti' mod='mlimport_ideal'}" />
        {* All categories *}
        <br /> <br />
        <p>2 - tai HOME kategorija.</p>
       
        <table class="table">
            <thead>
            <th> Pavadinimas</th>
            <th> Kategorijos</th>
            </thead>
            {foreach from=$xml_cats key=k item=v}
                <tr>
                    <td>{$v.name|escape:'htmlall':'UTF-8'}</td>
                    <td>
                        <input type="hidden" name="mlidealcat_name_{$k}" value="{$v.name}"/>
                        <input type="text" name="mlidealcat_value_{$k}" value="{$v.vals}" placeholder="Kategorijų IDS. PVZ: 3,4,5,15,52"/>
                    </td>
                </tr>
            {/foreach}
        </table>
        <input type="submit" class="btn button large" value="{l s='Išsaugoti' mod='mlimport_ideal'}" />
</div>

<div class="panel">
    <h3>Logs</h3>

    <div id="reports" class="mainblock-xml">
        <div class="margin-form">
            <div id="wooYayMessage" class="info-zurnalas">
                {if !empty($rl)}
                    {$rl}
                {/if}
            </div>
        </div>
    </div>

</div>