<?php

require_once "AbstractProduct.php";

class MwHandelProduct extends AbstractProduct
{
    private $categoryId = null;
    private $categories = null;
    private $imagesArray = null;

    static function getPrefix()
    {
        return "hd-";
    }

    function isValid()
    {
        return isset($this->backingArray[0]) && $this->backingArray[0] > 0;
    }

    function getFeaturesArray()
    {
        return [];
    }

    function getCategories()
    {
        $this->getCategoryId();
        return $this->categories;
    }

    function getCategoryId()
    {
        if ($this->categoryId == null)
        {
            $sql = 'SELECT selected_cat FROM ' . _DB_PREFIX_ . 'mlimport_ideal_cats WHERE category_code LIKE "' . trim($this->backingArray[3]) . '" ';
            $res = Db::getInstance()->getValue($sql);

            if ($res)
            {
                $this->categories = explode(',', $res);
                $this->categoryId = end($catsids);
            }

            // If no categories just assign default ID
            if (!Validate::isInt($this->categoryId))
            {
                $this->categoryId = 2;
                $this->categories = array(2);
            }
        }
        return $this->categoryId;
    }

    function getImagesArray()
    {
        if ($this->imagesArray == null)
        {
            $this->imagesArray = [];
            $this->imagesArray[] = $this->backingArray[20];
        }

        return $this->imagesArray;
    }

    function getName()
    {
        return "{$this->backingArray[5]} {$this->backingArray[1]} {$this->getCode()}";
    }

    function getMLSKU()
    {
        return $this->backingArray[0];
    }

    function getQuantity()
    {
        return $this->backingArray[22];
    }

    public function getManufacturerId()
    {
        return (int)MLimModifiedHelpers::addManufacturer($this->backingArray[5]);
    }

    public function getSupplierId()
    {
        return (int)MLimModifiedHelpers::addSupplier($this->backingArray[1]);
    }

    function getPrice()
    {
        return $this->backingArray[4];
    }

    function getCode()
    {
        return static::getPrefix()."{$this->backingArray[0]}";
    }
}