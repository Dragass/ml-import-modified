<?php
/**
 * Created by PhpStorm.
 * User: mgrid
 * Date: 2017-06-15
 * Time: 18:54
 */


/**
 * Class AbstractProduct
 *
 * An abstract product defines how a product in general should look. Provides
 * several abstract methods and a lot of non abstract versions of them to
 * simplify structure related to handling a product.
 */
abstract class AbstractProduct
{
    protected $backingArray = [];
    private $productId = null;

    function __construct($array)
    {
        if ($array == null)
            throw new \Exception("Invalid array!");
        $this->backingArray = $array;
    }

    static function getPrefix()
    {
        return null;
    }

    function getImagesArray()
    {
        return null;
    }

    function getReference()
    {
        return null;
    }

    /**
     * @return Product
     */
    function toNewPrestaShopProduct()
    {
        $myProduct = new Product();
        $myProduct->active = 0;
        if (Validate::isPrice($this->getPrice()))
            $myProduct->price = $this->getModifiedPrice();
        else
            $myProduct->price = 0.00;
        $myProduct->reference = $this->getReference(); // Part code with IL-5DIGITS;
        $myProduct->mlsku = $this->getMLSKU(); // Full product code from CSV;
        $myProduct->minimal_quantity = $this->getMinimalQuantity();
        $myProduct->quantity = (int)$this->getQuantity(); // warehouse stock int
        $myProduct->name = MLimModifiedHelpers::createMultiLangField($this->getName());
        $myProduct->link_rewrite = $this->getLinkRewrite();
        $myProduct->meta_description = $this->getMetaDescription();
        $myProduct->meta_title = $this->getMetaTitle();
        $myProduct->description = $this->getDescription();
        $myProduct->available_now = $this->getAvailableNow();
        $myProduct->available_later = $this->getAvailableLater();
        $myProduct->date_add = $this->getDateAdd();
        $myProduct->date_upd = $this->getDateUpd();
        $myProduct->id_category_default = $this->getCategoryId();
        $myProduct->id_manufacturer = $this->getManufacturerId();
        $myProduct->id_supplier = $this->getSupplierId();
        return $myProduct;
    }

    function getPrice()
    {
        return null;
    }

    function getModifiedPrice()
    {
        return $this->getPrice();
    }

    function getCode()
    {
        return null;
    }

    function getMLSKU()
    {
        return null;
    }

    function getMinimalQuantity()
    {
        return 1;
    }

    function getQuantity()
    {
        return null;
    }

    function getName()
    {
        return null;
    }

    function getLinkRewrite()
    {
        return MLimModifiedHelpers::createMultiLangField(Tools::link_rewrite($this->getName()));
    }

    function getMetaDescription()
    {
        return MLimModifiedHelpers::createMultiLangField($this->getName());
    }

    function getMetaTitle()
    {
        return MLimModifiedHelpers::createMultiLangField($this->getName());
    }

    function getDescription()
    {
        return null;
    }

    function getAvailableNow()
    {
        return MLimModifiedHelpers::createMultiLangField('1 - 2 d.d.');
    }

    function getAvailableLater()
    {
        return MLimModifiedHelpers::createMultiLangField('5 - 10 d.d.');
    }

    function getWeight()
    {
        return 1;
    }

    function getDateAdd()
    {
        return date('Y-m-d H:i:s');
    }

    function getDateUpd()
    {
        return date('Y-m-d H:i:s');
    }

    function getCategoryId()
    {
        return null;
    }

    /**
     * @return null|int
     */
    public function getManufacturerId()
    {
        return null;
    }

    /**
     * @return null|int
     */
    public function getSupplierId()
    {
        return null;
    }

    abstract function isValid();

    function update()
    {
        $this->toUpdatePrestaShopProduct()->update();
    }

    /**
     * @return Product
     */
    function toUpdatePrestaShopProduct()
    {
        $myProduct = new Product($this->getProductId());
        $myProduct->price = $this->getModifiedPrice();
        $myProduct->date_upd = date('Y-m-d H:i:s');
        return $myProduct;
    }

    function getProductId()
    {
        if ($this->productId == null)
            $this->productId = MLimModifiedHelpers::getIdByCode($this->getReference());
        return $this->productId;
    }

    function isInDatabase()
    {
        return $this->getProductId();
    }

    abstract function getCategories();

    abstract function getFeaturesArray();
}