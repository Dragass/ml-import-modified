<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Mantas Lukosius <info@elpresta.eu>
 *  @copyright 2015-2016 elPresta
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

include(dirname(__FILE__) . '/../../config/config.inc.php');
include(dirname(__FILE__) . '/../../init.php');
include(dirname(__FILE__) . '/mlgitoma.php');

$start = microtime(true);
$mlgitoma    = new Mlgitoma();

if (Tools::getValue('update') == 'gitoma_price') {

    // Main data
    $start = microtime(true);
    $fatal_errors = array();
    $dtime = Configuration::get('ml_gitaup_dtime');
    $today = date("Y-m-d H:i:s");
    $timediff = strtotime($today) - strtotime($dtime);
    $filename = 'gitoma_prices.xml';
    $prefix = 'git';
    $lastUpdateTime = Configuration::get('ml_gitaup_lastUpdate');
    $lastUpdateDiff = strtotime($today) - strtotime($lastUpdateTime);

    // Context for multistore
    if (Shop::isFeatureActive()) {
        Shop::setContext(Shop::CONTEXT_ALL);
    }

    // Check if first time or not
    if (empty($dtime)) {
        Configuration::updateValue('ml_gitaup_dtime', $today);
        echo 'Sveikiname! Pirmasis produkcijos atnaujinimas <br>';
        $dtime = Configuration::get('ml_gitaup_dtime');
    }

    if ($timediff > 86400) {
        echo 'Atsiunciamas duomenu failas... <br>';
        $mlgitoma->downloadDataUpdatePrices();
    }

    // Load data if possible
    if (file_exists($filename)) {
        $xml = simplexml_load_file($filename) or die("Error: neina nuskaityti XML");
    } else {
        $mlgitoma->downloadDataUpdatePrices();
        $xml = simplexml_load_file($filename) or die("Error: neina nuskaityti XML");
    }

    $dateFromDatabase = strtotime($lastUpdateTime);
    $dateTwelveHoursAgo = strtotime("-23 hours");
    $cont = false;
    if ($dateFromDatabase >= $dateTwelveHoursAgo) {
        echo 'Paskutini karta atnaujinta' . $lastUpdateTime.' <br /> Nuo praejusio atnaujinimo praejo maziau nei para. Sekantis atnaujinimas uz 24h.';
        die();
    } else {
        echo 'Atnaujinimas: ' . (!empty($xml) ? 'prasideda... <br>' : die('Klaida: neina nuskaityti XML.'));
        $lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $viso = count($xml);
        $tmpI = (int)Configuration::get('ml_gitaup_lastId');
        $cak = 0;
        $cont = true;
    }

    if ($cont == false) {
        die();
    } else {
        // Import data start
        $mlgitoma->updatePrices();
    }


}

if (Tools::getValue('update') == 'gitoma_update') {

    // Main data
    $start = microtime(true);
    $fatal_errors = array();
    $dtime = Configuration::get('ml_gitau_dtime');
    $today = date("Y-m-d H:i:s");
    $timediff = strtotime($today) - strtotime($dtime);
    $filename = 'gitoma_stock.xml';
    $prefix = 'git';
    $lastUpdateTime = Configuration::get('ml_gitau_lastUpdate');
    $lastUpdateDiff = strtotime($today) - strtotime($lastUpdateTime);

    // Context for multistore
    if (Shop::isFeatureActive()) {
        Shop::setContext(Shop::CONTEXT_ALL);
    }

    // Check if first time or not
    if (empty($dtime)) {
        Configuration::updateValue('ml_gitau_dtime', $today);
        echo 'Sveikiname! Pirmasis produkcijos atnaujinimas <br>';
        $dtime = Configuration::get('ml_gitau_dtime');
    }

    // Get new data
    if ($timediff > 86400) {
        echo 'Atsiunciamas duomenu failas... <br>';
        $mlgitoma->downloadDataUpdate();
    }


    // Load data if possible
    if (file_exists($filename)) {
        $xml = simplexml_load_file($filename) or die("Error: neina nuskaityti XML");
    } else {
        $mlgitoma->downloadDataUpdate();
        $xml = simplexml_load_file($filename) or die("Error: neina nuskaityti XML");
    }

    $dateFromDatabase = strtotime($lastUpdateTime);
    $dateTwelveHoursAgo = strtotime("-23 hours");
    $cont = false;

    if ($dateFromDatabase >= $dateTwelveHoursAgo) {
        echo 'Paskutini karta atnaujinta' . $lastUpdateTime.' <br /> Nuo praejusio atnaujinimo praejo maziau nei para. Sekantis atnaujinimas uz 24h.';
        die();
    } else {
        echo 'Atnaujinimas: ' . (!empty($xml) ? 'prasideda... <br>' : die('Klaida: neina nuskaityti XML.'));
        $lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $viso = count($xml);
        $tmpI = (int)Configuration::get('ml_gitau_lastId');
        $cont = true;

        echo '<br> Viso produktu: ' . $viso.' <hr />';
        echo 'Paskutinis fiksuotas produktas:' . $tmpI . '<br> <hr />';
    }

    if ($cont == false) {
        die('CONT FALSE');
    } else {
        // Import data start
        echo 'Prasideda atnaujinimas... <br />';
        $mlgitoma->importUpdate();
    }
}

$time_spent = round((microtime(true) - $start), 2);
echo 'Done. Check excecution statistics to get more info. Time spent: ' . round((microtime(true) - $start), 2) . ' s.';
