<?php

/**
 * Class AbstractWorker
 *
 * An abstract worker is a worker that handles underlying Product wrappers
 * and methods related to them: Updating, Creating, removing, etc.
 *
 */
abstract class AbstractWorker
{
    protected $total_not_exist = 0;
    protected $total_exist = 0;

    function work()
    {
        //$this->downloadFile();
        $csv_data = $this->readData();
        $this->importData($csv_data);
        $this->saveSummary();
        $this->check();
    }

    protected function downloadFile()
    {
        echo "Atsiunciamas duomenu failas... \r\n<br>";
        if ($this->getTimeDiff() > 86400 || !file_exists($this->getFullPlace()))
        {
            MLimModifiedHelpers::downloadData($this->getDataURL(), $this->getFullPlace());
        }
        else
        {
            echo "Duomenų failo ({$this->getDataURL()} atsisiųsti nereikia. \r\n<br>";
        }
    }

    protected function getTimeDiff()
    {
        $dtime = Configuration::get("mlimport_{$this->getKey()}_cdtime");
        $timediff = strtotime(date("Y-m-d H:i:s")) - strtotime($dtime);

        if (empty($dtime))
        {
            Configuration::updateValue("mlimport_{$this->getKey()}_cdtime", date("Y-m-d H:i:s"));
            echo "Pirmasis produkcijos atnaujinimas \r\n<br>";
        }

        return $timediff;
    }

    abstract function getKey();

    static function getFullPlace()
    {
        $type = static::getType();
        return __DIR__ . "/../data/$type.csv";
    }

    static function getType()
    {
        throw new Exception("needs override");
    }

    static function getDataURL()
    {
        throw new Exception("needs override");
    }

    /**
     * @return array of csv data
     */
    abstract function readData();

    protected function importData($csv_data)
    {
        if ($this->getLastUpdateTimeDiff() < 0)
            return;
        foreach ($csv_data as $key => $value)
        {
            $wrappedProduct = $this->getWrapper($value);
            $message = "Importuojamas produktas nr. $key: {$wrappedProduct->getName()}";
            MLimModifiedHelpers::addLog(0, $message);
            echo $message;
            $this->importProduct($key, $wrappedProduct);
        }
    }

    function getLastUpdateTimeDiff()
    {
        $lastUpdateTime = Configuration::get("ml_{$this->getKey()}_lastUpdate");
        $dateFromDatabase = strtotime($lastUpdateTime);
        $dateTwelveHoursAgo = strtotime("-12 hours");
        return $dateTwelveHoursAgo - $dateFromDatabase;
    }

    /**
     * @param $array
     * @return AbstractProduct;
     */
    protected abstract function getWrapper($array);

    /**
     * @param $i
     * @param $wrappedProduct AbstractProduct
     */
    function importProduct($i, $wrappedProduct)
    {
        if ($wrappedProduct->isValid())
        {
            if ($wrappedProduct->isInDatabase())
            {
                $this->total_exist++;
                if (Validate::isPrice($wrappedProduct->getPrice()))
                {
                    try
                    {
                        $wrappedProduct->update();
                        Configuration::updateValue("ml_{$this->getKey()}_lastId", $i);
                        MLimModifiedHelpers::addLog($wrappedProduct->getProductId(), "Price updated. Id product:" . $wrappedProduct->getProductId() . " <b>code:" . $wrappedProduct->getCode() . "</b>. Price:" . $wrappedProduct->getModifiedPrice() . " \r\n<br/>");
                    }
                    catch (Exception $e)
                    {
                        MLimModifiedHelpers::addLog($wrappedProduct->getProductId(), "Atnaujinimas nepavyko: {$e->getMessage()}");
                    }
                }
                else
                {
                    MLimModifiedHelpers::addLog($wrappedProduct->getProductId(), "PRODUCT UPDATE ERROR: Produkto kaina neteisinga: {$wrappedProduct->getPrice()}");
                    Configuration::updateValue("ml_{$this->getKey()}_lastId", $i);
                }
            }
            else
            {
                // START OF PRODUCT ADD
                $this->total_not_exist++;
                MLimModifiedHelpers::addLog(0,"Naujo produkto pridejimas: " . $wrappedProduct->getName() . " \r\n<br/>");

                $myProduct = $wrappedProduct->toNewPrestaShopProduct();

                try
                {
                    $myProduct->add();

                    MLimModifiedHelpers::addLog($myProduct->id, "PRODUCT CREATE: Produktas {$wrappedProduct->getName()} sukurtas!");
                    Configuration::updateValue("ml_{$this->getKey()}_lastId", $i);
                    $myProduct->updateCategories($wrappedProduct->getCategories());
                    MLimModifiedHelpers::addLog($myProduct->id, "Pridetos kategorijos produktui");
                    // Supplier data
                    //MLimModifiedHelpers::saveSupplier($myProduct);

                    // IMAGES

                    foreach ($wrappedProduct->getImagesArray() as $url)
                    {
                        MLimModifiedHelpers::addImage($url, $myProduct->id);
                    }
                    MLimModifiedHelpers::addLog($myProduct->id, "Pridetos paveikliukai produktui");
                    // FEATURES

                    foreach ($wrappedProduct->getFeaturesArray() as $feature)
                    {
                        MlimModifiedHelpers::addFeature($feature, $myProduct->id);
                    }
                    MLimModifiedHelpers::addLog($myProduct->id, "Pridetos featuresai produktui");
                    $myProduct->update();
                }
                catch (Exception $e)
                {
                    MLimModifiedHelpers::addLog($wrappedProduct->getProductId(), "Atnaujinimas nepavyko: {$e->getMessage()}");
                }
            }
        }
    }

    function saveSummary()
    {
        MLimModifiedHelpers::addLog(0, "Viso produktų rasta:" . $this->total_exist . "; Viso nerasta - " . $this->total_not_exist . " \r\n<br/>");
        Configuration::updateValue("ml_{$this->getKey()}_lastId", 0);
        Configuration::updateValue("ml_{$this->getKey()}_lastUpdate", date("Y-m-d H:i:s"));
        MLimModifiedHelpers::addLog(0, "Atnaujinimas baigtas. Sekantis atnaujinimas - po 24 val.");
    }

    function check()
    {
        MLimModifiedHelpers::addLog(0, "Peržiūrimos prekės, kurias atjungti.");
        $products_ids = Db::getInstance()->getValue("SELECT GROUP_CONCAT(p.`id_product`) AS id_product
			FROM `" . _DB_PREFIX_ . "product` p
			WHERE p.`date_upd` < (NOW() - INTERVAL 2 DAY) AND p.`active` = 1 AND p.`reference` LIKE \"%IL-%\"
			");

        $products_all = explode(",", $products_ids);
        $total_off = 0;

        foreach ($products_all as $product_id)
        {
            if ($product_id > 0)
            {
                $total_off++;
                $res1 = Db::getInstance()->Execute("UPDATE `" . _DB_PREFIX_ . "product` SET `active` = 0 WHERE `id_product` = " . (int)$product_id . " ;");
                $res2 = Db::getInstance()->Execute("UPDATE `" . _DB_PREFIX_ . "product_shop` SET `active` = 0 WHERE `id_product` = " . (int)$product_id . " ;");

            }
        }
        MLimModifiedHelpers::addLog(0, " Viso: $total_off");
    }

    static function getSeparator()
    {
        return ",";
    }

    static function getSkipLines()
    {
        return 1;
    }
}