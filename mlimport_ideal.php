<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Mantas Lukosius <info@elpresta.eu>
 * @copyright 2015-2017 elPresta
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

if (!defined('_PS_VERSION_'))
{
    exit;
}

/*
 * Export module for ideal supplier
 */

// Load extra class
require_once(dirname(__FILE__) . "/MLimModifiedHelpers.php");

class Mlimport_ideal extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'mlimport_ideal';
        $this->tab = 'administration';
        $this->version = '2.0.0';
        $this->author = 'Mantas Lukosius';
        $this->need_instance = 0;
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Ideal LUX XML-modifikuotas');
        $this->description = $this->l('Ideal LUX XML integracija pritaikyta ivariems CSV formatams');
    }

    public function install()
    {
        $this->installDatabase();
        Configuration::updateValue('mlimport_ideal_data', 'ftp://ideal-lux_clienti:clidll5896xd@www.ideal-lux.com/giacenzeECommerce.csv');

        return parent::install() &&
            $this->registerHook('backOfficeHeader');
    }

    public function installDatabase()
    {
        $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'mlimport_ideal` (
            `id_mlimport_ideal` INT(11) NOT NULL AUTO_INCREMENT,
            `id_product` INT(11),
            `log_info` VARCHAR(512),
            `date_add` VARCHAR(128),
            PRIMARY KEY  (`id_mlimport_ideal`)
        ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

        if (Db::getInstance()->execute($sql) == false)
        {
            return false;
        }


        // Table for logs
        $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'mlimport_ideal_cats` (
                `id_mlimport_ideal_cat` INT(11) NOT NULL AUTO_INCREMENT,
                `category_code` VARCHAR(512),
                `selected_cat` VARCHAR(256),
                `date_add` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY  (`id_mlimport_ideal_cat`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

        if (Db::getInstance()->execute($sql) == false)
        {
            return false;
        }

    }

    public function uninstall()
    {

        return parent::uninstall();
    }

    public function getContent()
    {

        $output = '';

        // Main form submit
        if (((bool)Tools::isSubmit('submitMlimport_idealModule')) == true)
        {
            $this->postProcess();
        }

        // Categories submit
        if (Tools::getValue('submitIdealCats') && (int)Tools::getValue('submitIdealCats') == 1)
        {
            $values = Tools::getAllValues();

            $categories_mapping = array();
            foreach ($values as $key => $value)
            {

                // Catch all values
                if (strpos($key, 'mlidealcat_name') !== false)
                {
                    // All variables inside key
                    $categories_mapping[] = $key;
                }
            }

            foreach ($categories_mapping as $cm)
            {
                $cm_array = explode("_", $cm);
                $id_cati = (int)$cm_array[2];

                $cat_name = Tools::getValue('mlidealcat_name_' . $id_cati);
                $cat_assigned_ids = Tools::getValue('mlidealcat_value_' . $id_cati);

                if (!empty($cat_name))
                {
                    $sql = 'SELECT id_mlimport_ideal_cat FROM ' . _DB_PREFIX_ . 'mlimport_ideal_cats WHERE category_code = "' . pSQL($cat_name) . '" ';
                    $existCategory = (int)Db::getInstance()->getValue($sql);

                    if (!$existCategory)
                    {
                        Db::getInstance()->insert('mlimport_ideal_cats', array(
                            'category_code' => pSQL($cat_name),
                            'selected_cat' => pSQL($cat_assigned_ids),
                        ));
                    }
                    else
                    {
                        $table = 'mlimport_ideal_cats';
                        $data = array(
                            'selected_cat' => pSQL($cat_assigned_ids),
                        );

                        $where = 'category_code = "' . pSQL($cat_name) . '" ';
                        Db::getInstance()->update($table, $data, $where, $limit = 1, $null_values = true);
                    }

                }
            }

            $output .= $this->displayConfirmation('Kategorijų suvedžiojimas - išsaugotas.');

        }

        $xml_cats = $this->getCategoriesFromXml();


        $uri = Tools::getHttpHost(true) . __PS_BASE_URI__;
        $add_url = $uri . 'modules/' . $this->name . '/mlimport_ideal_add.php';

        (function_exists('curl_init')) ? $jecurl = '<span class="bold" style="color:green">CURL is available&nbsp;&nbsp;</span>' : $jecurl = '<span class="bold" style="color:red">CURL IS NOT AVAILABLE !!!</span>';

        // Get selected categories
        $selected_xml_categories = $this->getSelectedCategories();


        $this->context->smarty->assign('module_dir', $this->_path);
        $this->context->smarty->assign('selected_xml_categories', $selected_xml_categories);
        $this->context->smarty->assign('add_url', $add_url);
        $this->context->smarty->assign('ai', Configuration::get('PS_ALLOW_HTML_IFRAME'));
        $this->context->smarty->assign('footime', ini_get("max_execution_time"));
        $this->context->smarty->assign('sl', ini_get("PS_PRODUCT_SHORT_DESC_LIMIT"));
        $this->context->smarty->assign('jecurl', $jecurl);
        $this->context->smarty->assign('xml_cats', $xml_cats);

        $rl = $this->printLog();
        $this->context->smarty->assign('rl', $rl);


        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');

        return $output;
    }

    public function getCategoriesFromXml()
    {

        // Must load from URL otherwise false
        $data_path = Configuration::get('mlimport_ideal_data');

        if (MLimModifiedHelpers::downloadData($data_path, 'categories'))
        {
            $full_place = __DIR__ . '/data/categories.csv';
            $xml = MLimModifiedHelpers::readCsvFile($full_place, ';');

            // Unique key = Gruppo Prodotto = array 5

            $categories_array_temp = array();
            $categories_array = array();
            // 0 header line
            for ($i = 1; $i <= count($xml); $i++)
            {
                // Catch all categories
                if (isset($xml[$i][5]))
                {
                    $cat_name = (string)$xml[$i][5];

                    if (!in_array($cat_name, $categories_array_temp) && !empty($cat_name))
                    {
                        $categories_array_temp[] = $cat_name;
                        $categories_array[$i]['name'] = $cat_name;

                        // Check if has any value assigned
                        $sql = 'SELECT selected_cat 
                               FROM ' . _DB_PREFIX_ . 'mlimport_ideal_cats 
                               WHERE category_code = "' . pSQL($cat_name) . '" ';
                        $res = Db::getInstance()->getValue($sql);

                        // If any category assigned - push to array
                        if ($res)
                        {
                            $categories_array[$i]['vals'] = $res;
                        }
                        else
                        {
                            // Default category if any category assigned - home 2;
                            $categories_array[$i]['vals'] = 2;
                        }

                    }
                }
            }

            return $categories_array;
        }

        return false;
    }

    public function getSelectedCategories()
    {
        $selected_categories = array();

        $sql = 'SELECT category_code, selected_cat FROM ' . _DB_PREFIX_ . 'mlimport_ideal_cats WHERE 1';
        $res = Db::getInstance()->executeS($sql);

        if ($res)
        {
            foreach ($res as $r)
            {
                $selected_categories[$r['category_code']] = $r['selected_cat'];
            }

            return $selected_categories;
        }

        return $selected_categories;

    }

    public function printLog()
    {
        $out = '
        <table border="1" cellpadding="0" cellspacing="0" style="width:100%;">';

        $result = Db::getInstance()->ExecuteS("SELECT * FROM `" . _DB_PREFIX_ . "mlimport_ideal`  ORDER BY `id_mlimport_ideal` DESC LIMIT 1000");

        if (Db::getInstance()->NumRows())
        {
            foreach ($result as $row)
            {
                $out .= '<tr bgcolor = "#FFFFFF"><td>' . $row["id_mlimport_ideal"] . '</td><td>' . $row["log_info"] . '</td><td>' . $row["date_add"] . '</td></tr>';
            }
            return $out .= '</table>';
        }
    }

    /**
     * @Deprecated use helpers instead
     * @param $url
     * @param $id_product
     */
    public function addImage($url, $id_product)
    {
        MLimModifiedHelpers::addImage($url, $id_product);
    }

    /**
     * @param $str
     * @param $allowedLenght
     * @return string
     * @deprecated Use helpers instead
     */
    public function replaceDisallowed($str, $allowedLenght)
    {
        return MLimModifiedHelpers::replaceDisallowed($str, $allowedLenght);
    }

    /*
     * Get IDS to asign
     */
    public function getCategoriesIds($category_arr)
    {

        $ids_array = array();
        if ($category_arr)
        {
            foreach ($category_arr as $key => $value)
            {
                $sql = 'SELECT selected_cat FROM ' . _DB_PREFIX_ . 'mlimport_ideal_cats WHERE category_code = ' . (int)$key;
                $sc = Db::getInstance()->getValue($sql);

                if ($sc)
                {
                    $temp_arr = explode(',', $sc);
                    foreach ($temp_arr as $val)
                    {
                        $ids_array[] = $val;
                    }
                }
            }

            return $ids_array;
        }

        return false;

    }

    /*
     * Create categories
     */
    public function getCategoriesXml($category_arr)
    {
        $lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $catky = array();

        foreach ($category_arr as $categories_string)
        {
            $categories = explode('/', (string)$categories_string);
            $nr = 0;
            $catsuma = count($categories);
            $parent = 156;

            for ($c = 0; $c < $catsuma; $c++)
            {
                $catid = Category::searchByNameAndParentCategoryId($lang, $categories[$c], $parent);
                if ($catid && !empty($catid))
                {
                    $id = $catid['id_category'];
                    $parent = $id;
                }
                else
                {
                    $object = new Category();
                    $object->name = $this->createMultiLangField((string)$categories[$c]);
                    $object->id_parent = $parent;
                    $category_name = (string)$categories[$c];
                    $object->link_rewrite = $this->createMultiLangField(Tools::link_rewrite((string)$category_name));
                    $object->add();
                    $id = $object->id;
                    $parent = $id;
                }
                $catky[] = $id;
            }
            $nr++;

        }

        return $catky;
    }

    /**
     * @deprecated use MLimModifiedHelpers instead
     * @param $field
     * @return array
     */
    public function createMultiLangField($field)
    {
        return MLimModifiedHelpers::createMultiLangField($field);
    }

    /**
     * @param $ft
     * @param $id_product
     * @deprecated Use helpers instead
     */
    public function addFeature($ft, $id_product)
    {

        MLimModifiedHelpers::addFeature($ft, $id_product);


    }

    /**
     * @param $savybe
     * @param $id_lang
     * @return false|null|string
     * @deprecated use helpers instead
     */
    public function getFeatureValueId($savybe, $id_lang)
    {
        return MLimModifiedHelpers::getFeatureValueId($savybe, $id_lang);
    }

    public function getFeatureValueByName($feature, $lang)
    {
        $sql = 'SELECT id_feature FROM ' . _DB_PREFIX_ . 'feature_lang WHERE id_lang = ' . $lang . ' AND `name` = \'' . pSQL($feature) . '\'';
        $idfeature = (int)Db::getInstance()->getValue($sql);

        return $idfeature;
    }

    public function addFeatureValueN($id_feature, $name)
    {
        return MLimModifiedHelpers::addFeatureValueN($id_feature, $name);
    }

    /**
     * @param $gamintojas
     * @return bool|int
     * @deprecated use helpers instead
     */
    public function addManufacturer($gamintojas)
    {
        return MLimModifiedHelpers::addManufacturer($gamintojas);
    }

    /**
     * @param $xSUPPLIER
     * @return bool|int
     * @deprecated use helpers instead
     */
    public function addSupplier($xSUPPLIER)
    {
        return MLimModifiedHelpers::addSupplier($xSUPPLIER);
    }

    /**
     * @deprecated use MLimModifiedHelpers instead
     * @param $id_entity
     * @param null $id_image
     * @param $url
     * @param string $entity
     */
    public function copyImg($id_entity, $id_image = null, $url, $entity = 'products')
    {
        MLimModifiedHelpers::copyImg($id_entity, $id_image, $url, $entity);
    }

    /**
     * @deprecated use MLimModifiedHelpers instead
     * @param $url
     * @return
     */
    public function file_get_contents_curl($url)
    {
        return MLimModifiedHelpers::fileGetContentsCurl($url);
    }

    public function get_web_page($url)
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // don't return headers
            CURLOPT_FOLLOWLOCATION => false, // follow redirects
            CURLOPT_ENCODING => "", // handle all encodings
            CURLOPT_USERAGENT => "spider", // who am i
            CURLOPT_AUTOREFERER => true, // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
            CURLOPT_TIMEOUT => 120, // timeout on response
            CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        curl_close($ch);

        return $content;
    }


    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('configure') == $this->name)
        {
            $this->context->controller->addCSS($this->_path . 'views/css/mlimport_ideal.css');
        }
    }

}
