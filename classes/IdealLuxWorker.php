<?php

/**
 * Created by PhpStorm.
 * User: mgrid
 * Date: 2017-06-16
 * Time: 14:20
 */

require_once(dirname(__FILE__) . "/AbstractWorker.php");
require_once(dirname(__FILE__) . "/../model/IdeaLuxProduct.php");

class IdealLuxWorker extends AbstractWorker
{
    static function getType()
    {
        return "idea-lux-products";
    }

    function readData()
    {
        $csv_data = MLimModifiedHelpers::readCsvFile($this->getFullPlace(), ";");
        $total_products = count($csv_data);
        $lastId = (int)Configuration::get("ml_{$this->getKey()}_lastId");
        $message = "Viso produktu sarase: $total_products. Paskutinis fiksuotas ID: $lastId. \r\n<br />";
        echo $message;
        MLimModifiedHelpers::addLog(0, $message);
        return $csv_data;
    }

    function getKey()
    {
        return "ideal";
    }

    static function getDataURL()
    {
        return "ftp://ideal-lux_clienti:clidll5896xd@www.ideal-lux.com/giacenzeECommerce.csv";
    }

    /**
     * @param $array
     * @return AbstractProduct;
     */
    protected function getWrapper($array)
    {
        return new IdeaLuxProduct($array);
    }

    static function getSeparator()
    {
        return ";";
    }
}